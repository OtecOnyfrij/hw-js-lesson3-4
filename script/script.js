/*ДЗ 1
Використовуючи цикли намалюйте: 
1 Порожній прямокутник
2 Заповнений 
3 Рівносторонній трикутний 
4 Прямокутний трикутник 
5 Ромб
*/

document.write("<div class='footer'>" + "Домашні роботи урок 3-4" + "</div>" + "</br>")
document.write("<div class='dz_1'>" + "ДЗ 1:\
Використовуючи цикли намалюйте:\
Порожній прямокутник. \
Заповнений прямокутник. \
Рівносторонній трикутний. \
Прямокутний трикутник. \
Ромб." + "</div>" + "</br>");

document.write("<div class='main'>")
//1 Порожній прямокутник
document.write("<div class='first'>");
document.write("<div>");
document.write("1 Порожній прямокутник &darr;" + "</br>" + "</br>");
document.write("</div>");
document.write("<div>");
let width = 30;
let height = 5;
for (let a = 0; a <= height; a++) {
    for (let b = 0; b <= width; b++) {
        if ((a === 0) || (a === height) || (b === 0) || (b === width)) {
            document.write("*");
        } else { document.write("&nbsp;") }
    }
    document.write("</br>");
}
document.write("</div>");
document.write("</div>");

//Заповнений Прямокутник

document.write("<div class='second'>");
document.write("<div>");
document.write("2 Заповнений прямокутник &darr;" + "</br>" + "</br>");
document.write("</div>");
document.write("<div>");
for (let width = 0; width < 30; ++width) {
    document.write("*");
}
document.write("</br>");
for (let width = 0; width < 30; ++width) {
    document.write("*");
}
document.write("</br>");
for (let width = 0; width < 30; ++width) {
    document.write("*");
}
document.write("</br>");
for (let width = 0; width < 30; ++width) {
    document.write("*");
}
document.write("</br>");
for (let width = 0; width < 30; ++width) {
    document.write("*");
}
document.write("</div>");
document.write("</div>");


//Рівносторонній трикутний 

document.write("<div class='third'>");
document.write("<div>");
document.write("3 Рівносторонній трикутний  &darr;" + "</br>" + "</br>");
document.write("</div>");
document.write("<div>");
for (let width = 0; width < 9; width = width + 2) {
    for (let space = (9 - width); space > 0; space = space - 2) {
        document.write("&nbsp;");
    }
    for (let height = 0; height <= width; height++) {
        document.write("*");
    }
    document.write("</br>");
}
document.write("</div>");
document.write("</div>");


//Прямокутний трикутник

document.write("<div class='fourth'>");
document.write("<div>");
document.write("4 Прямокутний трикутник  &darr;" + "</br>" + "</br>");
document.write("</div>");
document.write("<div>");
for (let width = 0; width < 9; width = width + 2) {

    for (let height = 0; height <= width; height++) {
        document.write("*")
    }
    document.write("</br>");
}
document.write("</div>");
document.write("</div>");


//Ромб


document.write("<div class='fifth'>")
document.write("<div>");
document.write("5 Ромб &darr;" + "</br>" + "</br>");
document.write("</div>");
document.write("<div>");
for (let width = 0; width < 9; width = width + 2) {
    for (let space = (9 - width); space > 0; space = space - 2) {
        document.write("&nbsp;");
    }
    for (let height = 0; height <= width; height++) {
        document.write("*");
    }
    document.write("</br>");
}
for (let width = 6; width >= 0; width = width - 2) {
    for (let space = (9 - width); space > 0; space = space - 2) {
        document.write("&nbsp;");
    }
    for (let height = 0; height <= width; height++) {
        document.write("*");
    }
    document.write("</br>");
}
document.write("</div>");
document.write("</div>");
document.write("</div>" + "</br>" + "</br>");


/* ДЗ 2
Створіть масив styles з елементами «Джаз» та «Блюз». 
Додайте «Рок-н-рол» до кінця.
Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною
Видаліть перший елемент масиву та покажіть його.
Вставте «Реп» та «Реггі» на початок масиву. */

document.write("<div class='dz_2'>" + "ДЗ-2: Створіть масив styles з елементами «Джаз» та «Блюз».\
Додайте «Рок-н-рол» до кінця.\
Замініть значення всередині на «Класика».\
Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною.\
Видаліть перший елемент масиву та покажіть його.\
Вставте «Реп» та «Реггі» на початок масиву." + "</div>" + "</br>");
document.write("<div class='arr'>");

//Створив масив з елементами «Джаз» та «Блюз».
document.write("1) Створив масив з елементами «Джаз» та «Блюз»." + "</br>");
var styles = ["«Джаз»", " «Блюз»"];
document.write("<div class='arr1'>" + styles + "</div>" + "</br>");
//document.write(styles.length); - показати довжину масиву.

//додав в кінець масиву «Рок-н-рол» (.push).
document.write("2) Додав в кінець масиву «Рок-н-рол»." + "</br>");
styles.push(" «Рок-н-рол»");
document.write("<div class='arr2'>" + styles + "</div>" + "</br>");

//замінив значення всередині масиву на «Класика»
document.write("3) Замінив значення всередині масиву на «Класика».\
" + "</br>" + "Працює для будь-якої довжини масиву" + "</br>");
styles.splice(styles.length / 2, 1, "«Класика»");
document.write("<div class='arr3'>" + styles + "</div>" + "</br>");

//видалив перший елемент масиву та показав його
document.write("4) Видалив перший елемент масиву та показав його." + "</br>");
let lostElement = styles.splice(0, 1, "");
document.write("<div class='arr4'>" + lostElement + "</div>" + "</br>");

//додав на початок масиву «Реп» та «Реггі»  (.unshift).
document.write("5) Додав на початок масиву «Реп» та «Реггі»." + "</br>");
styles.unshift("«Реп»", " «Реггі»");
document.write("<div class='arr5'>" + styles + "</div>" + "</br>");


document.write("</div>");